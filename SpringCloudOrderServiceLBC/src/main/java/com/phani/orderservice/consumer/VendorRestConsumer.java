package com.phani.orderservice.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class VendorRestConsumer {
	
	//1. Autowire the LoadBalancer Client
	@Autowired
	private LoadBalancerClient  loadBalancerClient;
	
	//2. Define one process to find/bind the process
	public String getVendorData() {
		
		//3. Goto Eureka with serviceId and getServiceInstances
		ServiceInstance serviceInstance = loadBalancerClient.choose("VENDOR-SERVICE");
		
		//4. Read URI and add Path
		String url = serviceInstance.getUri()+"/vendor/msg";
		
		//5. use RestTemplate
		RestTemplate restTemplate = new RestTemplate();
		
		//6. make call and get response
		ResponseEntity<String> resp = restTemplate.getForEntity(url, String.class);
		
		//7. return response body
		return resp.getBody();
	}
}
