package com.phani.orderservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.phani.orderservice.consumer.VendorRestConsumer;

@RestController
@RequestMapping("/order")
public class OrderRestController {
	
	@Autowired
	private VendorRestConsumer vendorRestConsumer;
	
	@GetMapping("/info")
	public String getOrderInfo() {
		return "FROM ORDER and " + vendorRestConsumer.getVendorData();
	}
}
